<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * 購物車
 */
class Cart extends Model
{
    protected $guarded = [];
}
