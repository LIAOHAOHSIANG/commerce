<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * 地址
 */
class Address extends Model
{
    public $table = "address";

}
